// @flow-weak

const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const path = require('path');
const _ = require('lodash');

function isIsogram (word : string) : boolean {
  return word.length > 0 && word.length === _.uniq(word.toLowerCase().split('')).length;
}

function reduceToIsograms (words : string[]) : string[] {
  return _.filter(words, isIsogram);
}

async function readIsograms (filepath : string) : Promise<string[]> {
  const basename = path.basename(filepath);
  console.log(`Loading: ${basename}`);

  const data = await fs.readFileAsync(filepath, { encoding: 'utf8' });
  const words = data.replace(/\r\n/g, '\n').split('\n');
  const isograms = reduceToIsograms(words);

  console.log(`Loaded: ${basename} - ${isograms.length} words`);
  return isograms;
}

const WORD_LISTS = {
  '3': 'words3.txt',
  '4': 'words4.txt',
  '5': 'words5.txt'
};

// This promisy stuff is messing with my flow. Doesn't want to await worth a damn either.
async function main () {
  console.log('Initializing...');
  const wordLists : {[key : string] : Array<string>} = {};

  wordLists['3'] = await readIsograms(path.resolve(`${__dirname}/data/${WORD_LISTS['3']}`));
  wordLists['4'] = await readIsograms(path.resolve(`${__dirname}/data/${WORD_LISTS['4']}`));
  wordLists['5'] = await readIsograms(path.resolve(`${__dirname}/data/${WORD_LISTS['5']}`));

  console.log('Dictionaries: ', Object.keys(wordLists));
  console.log('Initialized...');
}

main();
