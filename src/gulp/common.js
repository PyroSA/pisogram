/* @flow weak */

'use strict';

const _ = require('lodash');

const gulp = require('gulp');

const babel = require('gulp-babel');
const gulpBower = require('gulp-bower');
const concat = require('gulp-concat');
const cssmin = require('gulp-clean-css');
const eslint = require('gulp-eslint');
const gutil = require('gulp-util');
const gulpif = require('gulp-if');
const ignore = require('gulp-ignore');
const istanbul = require('gulp-istanbul');
const karma = require('gulp-karma');
const mocha = require('gulp-mocha');
const newer = require('gulp-newer');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');

const path = require('path');

const isparta = require('isparta');

const reporter = require('./reporters').mocha;

const isProd = function () {
  return process.env.NODE_ENV === 'test' || process.env.NODE_ENV === 'production';
};

const errCb = function (err) {
  if (err) {
    gutil.log(gutil.colors.red(err.stack || err.message || err));
  }

  if (process.env.GULP_WATCH || process.env.GULP_FAILSAFE) {
    if (typeof this !== 'undefined' && this !== null) {
      if (typeof this.emit === 'function') {
        this.emit('end');
      }
    }
  }
};

const exitCb = function (err) {
  if (err) {
    gutil.log(gutil.colors.red(err.stack || err.message || err));
  }

  if (process.env.GULP_WATCH || process.env.GULP_FAILSAFE) {
    if (typeof this !== 'undefined' && this !== null) {
      if (typeof this.emit === 'function') {
        this.emit('end');
      }
    }
  }
};

const copyNewer = function (dest, src) {
  return gulp
    .src(src)
    .pipe(newer(dest))
    .pipe(gulp.dest(dest));
};

const cssConcat = function (file, src) {
  return gulp.src(src)
    .pipe(newer('dist/public/styles/' + file)) // eslint-disable-line  prefer-template
    .pipe(gulpif(isProd(), cssmin({
      keepBreaks: true
    })))
    .pipe(concat(file))
    .pipe(gulp.dest('dist/public/styles/'));
};

const bower = function (dest) {
  const to = dest || 'bower';
  return gulpBower({ directory: to })
    .pipe(gulp.dest(to));
};

const babelClient = function (file, dest, src) {
  return gulp
    .src(src)
    .pipe(newer('' + dest + file)) // eslint-disable-line prefer-template
    .pipe(eslint({}))
    .pipe(eslint.format())
    .pipe(gulpif(!isProd(), sourcemaps.init()))
    .pipe(babel({
      babelrc: path.join(process.cwd(), '.babelrc')
    }))
    .on('error', errCb)
    .pipe(gulpif(isProd(), uglify()))
    .pipe(concat(file))
    .pipe(gulpif(!isProd(), sourcemaps.write('.', {
      sourceRoot: '/scripts/',
      sourceMappingURLPrefix: '/scripts/'
    })))
    .pipe(gulp.dest(dest));
};

const babelMocha = function (cb, src, spec, _options) {
  const options = _options || {};

  require('babel-core/register');

  process.env.NODE_ENV = 'test';

  if (options.noCoverage) {
    return runMocha(spec);
  }

  gulp
    .src(src)
    .pipe(istanbul({
      instrumenter: isparta.Instrumenter
    }))
    .pipe(istanbul.hookRequire())
    .on('finish', function () {
      runMocha(spec)
        .pipe(istanbul.writeReports({
          dir: './coverage',
          reporters: ['lcovonly'],
          reportOpts: {
            lcovonly: {
              dir: './coverage',
              file: 'server.lcov'
            }
          }
        }))
        .on('end', cb);
    })
    .on('error', exitCb);
};

const babelNode = function (dest, src) {
  return gulp
    .src(src)
    .pipe(newer('' + dest)) // eslint-disable-line prefer-template
    .pipe(eslint({}))
    .pipe(eslint.format())
    .pipe(babel({
      babelrc: path.join(process.cwd(), '.babelrc')
    }))
    .on('error', errCb)
    .pipe(gulp.dest(dest));
};

const jsConcat = function (file, src) {
  return gulp
    .src(src)
    .pipe(newer('dist/public/scripts/' + file)) // eslint-disable-line prefer-template
    .pipe(gulpif(isProd(), uglify()))
    .pipe(concat(file))
    .pipe(gulp.dest('dist/public/scripts/'));
};

const karmaServer = function (done, options) {
  new karma.Server(
    _.merge({
      configFile: path.join(process.cwd(), 'karma.conf.js'),
      singleRun: true
    }, options || {}), done).start();
};

const jsLint = function (src, opt) {
  return gulp
    .src(src)
    .pipe(eslint(opt || {}))
    .pipe(eslint.format());
};

const runMocha = function (spec) {
  return gulp
    .src(spec)
    .pipe(mocha({
      reporter: reporter,
      timeout: 30000
    }));
};

const sassMin = function (src, includePaths, _options) {
  const options = _options || {};
  const destDir = options.destDir || 'dist/public/styles';
  const destFile = options.destFile || 'client.css';
  const ignorePaths = options.ignore || ['**/src/client/styles/application.sass'];

  const task = gulp.src(src).pipe(ignore.include(true, ignorePaths));

  return task
    .pipe(sass({
      sourceComments: 'normal',
      outputStyle: 'nested',
      includePaths: includePaths || []
    }))
    .on('error', errCb)
    .pipe(ignore.exclude('*.css.map'))
    .pipe(gulpif(isProd(), cssmin({
      keepBreaks: true,
      keepSpecialComments: 0
    })))
    .pipe(concat(destFile))
    .pipe(gulp.dest(destDir));
};

const uglifyProd = function (file, dest, src) {
  return gulp
    .src(src)
    .pipe(gulpif(isProd(), uglify()))
    .pipe(concat(file))
    .pipe(gulp.dest(dest));
};

module.exports = {
  babelClient,
  babelMocha,
  babelNode,
  bower,
  copyNewer,
  cssConcat,
  jsConcat,
  jsLint,
  karmaServer,
  sassMin,
  uglifyProd
};
