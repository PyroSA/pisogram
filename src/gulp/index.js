/* @flow */

'use strict';

/* eslint no-console:0, prefer-arrow-callback:0 */

const common = require('./common');
const gulp = require('gulp');
const serial = require('run-sequence');
const path = require('path');
const del = require('del');
const through = require('through2');


gulp.task('clean', function (done) {
  return del(['coverage', 'dist'], done);
});


gulp.task('test-client-admin-quick', ['bower', 'css'], function (done) {
  return common.karmaServer(done, {
    noCoverage: true,
    configFile: path.join(process.cwd(), 'spec', 'client', 'admin', 'karma.conf.js')
  });
});

gulp.task('test-client-game-quick', ['bower', 'css'], function (done) {
  return common.karmaServer(done, {
    noCoverage: true,
    configFile: path.join(process.cwd(), 'spec', 'client', 'game', 'karma.conf.js')
  });
});

gulp.task('test-client-quick', ['test-client-admin-quick', 'test-client-game-quick']);


gulp.task('test-client-admin', ['bower', 'css'], function (done) {
  return common.karmaServer(done, {
    configFile: path.join(process.cwd(), 'spec', 'client', 'admin', 'karma.conf.js')
  });
});

gulp.task('test-client-game', ['bower', 'css'], function (done) {
  return common.karmaServer(done, {
    configFile: path.join(process.cwd(), 'spec', 'client', 'game', 'karma.conf.js')
  });
});

gulp.task('test-client', ['test-client-admin', 'test-client-game']);


gulp.task('copy-data', function () {
  return common.copyNewer('dist/data/', ['src/server/data/**/*']);
});

gulp.task('copy-favicon', function () {
  return common.copyNewer('dist/public/', ['src/client/favicon.ico']);
});

gulp.task('copy-images', function () {
  return common.copyNewer('dist/public/images/', ['src/client/images/**/*']);
});

gulp.task('copy-package.json', function () {
  return common.copyNewer('dist/', ['package.json']);
});

gulp.task('copy', [
  'copy-data',
  'copy-favicon',
  'copy-images',
  'copy-package.json'
]);


gulp.task('css', function () {
  return common.sassMin(
    ['src/client/styles/application.s?ss'],
    ['src/client/styles/vendor/'],
    { ignore: [] }
  );
});


gulp.task('html-admin', function () {
  return common.copyNewer('dist/public/admin/', ['src/client/admin/views/**/*']);
});

gulp.task('html-game', function () {
  return common.copyNewer('dist/public/game/', ['src/client/game/views/**/*']);
});

gulp.task('html', ['html-game', 'html-admin']);


gulp.task('js-client-admin', function () {
  return common.babelClient('cnc.js', 'dist/public/scripts/', ['src/client/admin/scripts/**/*.js']);
});

gulp.task('js-client-game', function () {
  return common.babelClient('game.js', 'dist/public/scripts/', ['src/client/game/scripts/**/*.js']);
});

gulp.task('js-client', ['js-client-admin', 'js-client-game']);


gulp.task('test-server-quick', function (done) {
  return common.babelMocha(done,
    ['src/server/scripts/**/*.js', 'src/shared/scripts/**/*.js'],
    ['spec/server/helpers/index.js', 'spec/shared/**/*.spec.js', 'spec/server/**/*.spec.js'],
    { noCoverage: true });
});

gulp.task('test-quick', function (cb) {
  return serial('test-server-quick', 'test-client-quick', cb);
});


gulp.task('test-server', function (done) {
  return common.babelMocha(done,
    ['src/server/scripts/**/*.js', 'src/shared/scripts/**/*.js'],
    ['spec/server/helpers/index.js', 'spec/shared/**/*.spec.js', 'spec/server/**/*.spec.js']);
});

gulp.task('test', function (cb) {
  return serial('test-server', 'test-client', cb);
});


gulp.task('bower', function () {
  return common.bower();
});

gulp.task('vendor-css', function () {
  return common.cssConcat('vendor.css', []);
});

gulp.task('vendor-js', function () {
  return common.jsConcat('vendor.js', [
    'bower/lodash/lodash.js',
    'bower/base64/base64.js',
    'bower/moment/moment.js',
    'node_modules/babel-polyfill/dist/polyfill.js',
    'bower/react/react.js',
    'bower/react/react-dom.js'
  ]);
});

gulp.task('vendor', ['bower'], function (done) {
  return serial('vendor-js', 'vendor-css', done);
});


gulp.task('js-server', function () {
  return common.babelNode('dist/', ['src/server/scripts/**/*.js']);
});

gulp.task('js-shared', function () { // TODO - Consider doing a babelClient version for clients
  return common.babelNode('dist/shared/', ['src/shared/scripts/**/*.js']);
});

gulp.task('js', ['js-client', 'js-server', 'js-shared']);


gulp.task('dot-only', function () {
  const regex = /(describe|context|it)\.only[ ]*\(/;

  gulp.src(['spec/**/*.js'])
    .pipe(through.obj(function (file, encoding, callback) {
      if (file.isStream()) {
        throw new Error('Stream not supported');
      }

      if (regex.test(String(file.contents))) {
        callback(new Error(file.path + ' Contains .only'), file);
        return;
      }

      callback();
    }));
});


gulp.task('lint-spec', function () {
  return common.jsLint(['spec/**/*.js']);
});

gulp.task('lint-src', function () {
  return common.jsLint(['src/**/*.js']);
});

gulp.task('lint', ['lint-spec', 'lint-src']);


gulp.task('default',
  ['css', 'html', 'js', 'copy', 'vendor']
);


gulp.task('watch', ['default'], function () {
  process.env.GULP_WATCH = 'true';

  gulp.watch(['src/client/styles/**/*.s?ss'], ['css']);

  gulp.watch(['src/client/game/views/**/*.html'], ['html-game']);
  gulp.watch(['src/client/scripts/**/*.js'], ['js-client-game']);

  gulp.watch(['src/client/admin/views/**/*.html'], ['html-admin']);
  gulp.watch(['src/client/admin/scripts/**/*.js'], ['js-client-admin']);

  gulp.watch(['src/shared/scripts/**/*.js'], ['js-client', 'js-shared']);
  gulp.watch(['src/server/scripts/**/*.js'], ['js-server']);
});
