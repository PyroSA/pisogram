module.exports = {
    "extends": "semistandard",
    "parser": "babel-eslint",
    "installedESLint": true,
    "plugins": [
        "standard",
        "promise",
        "flowtype"
    ],
    "rules" : {
      "no-multiple-empty-lines" : ['error', { "max": 2 }]
    }
};
