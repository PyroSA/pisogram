const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const COMMON_LOADERS = [
  { test: /\.png$/, loader: 'file-loader' },
  { test: /\.jpg$/, loader: 'file-loader' },
  { test: /\.js$/, loader: 'babel', exclude: /node_modules/ }
];

const BASE_SRC = path.join(__dirname, 'src');
const BASE_DIST = path.join(__dirname, 'dist');

const CLIENT_GAME = {
  name: 'game client',
  devtool: 'eval-source-map',
  entry: path.join(BASE_SRC, 'client', 'game', 'scripts', 'client.js'),
  output: {
    path: path.join(BASE_DIST, 'public', 'game'),
    filename: '[hash].js',
    publicPath: 'game/'
  },
  module: {
    loaders: COMMON_LOADERS.concat([
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract(
          'style', // backup loader when not building .css file
          'css!sass' // loaders to preprocess CSS
      )}
    ])
  },
  plugins: [
    new ExtractTextPlugin('[name].css'),
    function (compiler) {
      this.plugin('done', function (stats) {
        require('fs').writeFileSync(path.join(__dirname, 'tmp', 'game.generated.json'), JSON.stringify(stats.toJson()));
      });
    }
  ]
};

const CLIENT_ADMIN = {
  name: 'admin client',
  devtool: 'eval-source-map',
  entry: path.join(BASE_SRC, 'client', 'admin', 'scripts', 'client.js'),
  output: {
    path: path.join(BASE_DIST, 'public', 'cnc'),
    filename: '[hash].js',
    publicPath: 'cnc/'
  },
  module: {
    loaders: COMMON_LOADERS.concat([
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract(
          'style', // backup loader when not building .css file
          'css!sass' // loaders to preprocess CSS
      )}
    ])
  },
  plugins: [
    new ExtractTextPlugin('[name].css'),
    function (compiler) {
      this.plugin('done', function (stats) {
        require('fs').writeFileSync(path.join(__dirname, 'tmp', 'admin.generated.json'), JSON.stringify(stats.toJson()));
      });
    }
  ]
};

const SERVER = {
  name: 'server-side code',
  devtool: 'source-map',
  entry: path.join(BASE_SRC, 'server', 'scripts', 'app.js'),
  target: 'node',
  node: {
    console: true,
    __dirname: false
  },

  output: {
    path: BASE_DIST,
    filename: 'app.js',
    sourceMapFilename: '[file].map',
    libraryTarget: 'commonjs2'
  },

  // externals: /^[a-z\-0-9]+$/,

  module: {
    loaders: COMMON_LOADERS.concat(
      { test: /\.json$/, loader: 'json' }
    )
  },

  devServer: {
    contentBase: path.join('.', 'dist'),
    colors: true,
    historyApiFallback: true,
    inline: true
  },

  plugins: [
    function (compiler) {
      this.plugin('done', function (stats) {
        require('fs').writeFileSync(path.join(__dirname, 'tmp', 'server.generated.json'), JSON.stringify(stats.toJson()));
      });
    }
  ]
};

module.exports = [CLIENT_GAME, CLIENT_ADMIN, SERVER];
